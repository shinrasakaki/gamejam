extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		if sign($Position2D.position.x) == -1:
			$Position2D.position.x *= -1
		
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		if sign($Position2D.position.x) == 1:
			$Position2D.position.x *= -1

export (int, 0, 200) var push = 100
export (int, 0, 200) var inertia = 100


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP, false, 4, PI/4, false)
	
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider.is_in_group("bodies"):
			collision.collider.apply_central_impulse(-collision.normal * push)
	

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
