extends Area2D

func _ready():
	$Label.percent_visible = 0
	$Area2D.connect('body_entered', self, '_play_animation')

func _play_animation(body):
	$Area2D.disconnect('body_entered', self, '_play_animation')
	$AnimationPlayer.play('show')
